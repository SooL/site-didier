@extends('template')

@section('title')
Accueil
@endsection
@section('content')
<h1>Didier Marechal</h1>

<p>Ipsam vero urbem Byzantiorum fuisse refertissimam atque ornatissimam signis quis ignorat? Quae illi, exhausti sumptibus bellisque maximis, cum omnis Mithridaticos impetus totumque Pontum armatum affervescentem in Asiam atque erumpentem, ore repulsum et cervicibus interclusum suis sustinerent, tum, inquam, Byzantii et postea signa illa et reliqua urbis ornanemta sanctissime custodita tenuerunt.</p>

<div class="card-deck">
  @foreach($domaines as $domaine)
  <div class="card">
    <img class="card-img-top img-fluid" src="{{$domaine->url}}" alt="Card image cap" >
    <div class="card-body">
      <h5 class="card-title">{{$domaine->libelle}}</h5>
      <p class="card-text">Images prises au cours d'interventions.</p>
      <a type="button" href="{{route('domaine.show', ['domaine' => $domaine->id])}}" class="btn btn-primary">Voir</a>
    </div>
  </div>
    @endforeach
</div>
@stop
