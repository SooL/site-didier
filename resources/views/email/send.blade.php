@extends('template')

@section('title')
Contact
@stop
@section('content')

<section class="mb-4">

    <h2 class="h1-responsive font-weight-bold text-center my-4">Contactez moi</h2>

    <p class="text-center w-responsive mx-auto mb-5">Pour tout renseignement, merci de me contacter via ce formulaire ou par mail.</p>

    <div class="row">

        <div class="col-md-9 mb-md-0 mb-5">
          <form method="POST" action="{{ route('send') }}">
                  {{ csrf_field() }}

                <div class="row">

                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" name="nom" placeholder="Votre nom" class="form-control @error('nom') is-invalid @enderror">
                            @error('nom')
                            <div class="invalid-feedback">
                            {{ $errors->first('nom') }}
                          </div>
                          @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="md-form mb-0">
                            <input type="text" name="email" placeholder="Votre email" class="form-control @error('email') is-invalid @enderror">
                            @error('email')
                            <div class="invalid-feedback">
                            {{ $errors->first('email') }}
                          </div>
                          @enderror
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="md-form mb-0">
                          <br><input type="text" name="sujet" placeholder="Votre sujet" class="form-control @error('sujet') is-invalid @enderror">
                          @error('sujet')
                          <div class="invalid-feedback">
                          {{ $errors->first('sujet') }}
                        </div>
                        @enderror
                      </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">

                        <div class="md-form">
                          <br><input type="text" name="message" placeholder="Votre message" class="form-control @error('message') is-invalid @enderror"></textarea>
                          @error('message')
                          <div class="invalid-feedback">
                          {{ $errors->first('message') }}
                        </div>
                        @enderror
                        </div>
                    </div>
                </div>



            <div class="text-center text-md-left">
              <br><button type="submit" class="btn btn-secondary">Envoyer</button>
            </div>
            <div class="status"></div>
            </form>
        </div>

        <div class="col-md-3 text-center">
            <ul class="list-unstyled mb-0">
                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                    <p>Adresse de la boite</p>
                </li>

                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                    <p>+33 6 45 87 56 32</p>
                </li>

                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                    <p>didi.maré@gmail.com</p>
                </li>
            </ul>
        </div>

    </div>

</section>
<!--Section: Contact v.2-->
@stop
