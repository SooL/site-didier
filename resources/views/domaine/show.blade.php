@extends('template')
@section('title')
{{$domaine->libelle}}
@endsection
@section('content')

<div class="container">
    <div class="row" "zoom">
  @foreach($domaine->images as $i) <!-- ICI ;) -->
        <div class="col-4">
          <div class="image">
            <img src="{{$i->url}}" class="rounded" width=350px height=200px></img>
          </div>
        </div>
  @endforeach
    </div>
</div>
@stop
