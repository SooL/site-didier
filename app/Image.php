<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Domaine;

class Image extends Model
{
    public function domaine()
    {
        return $this->hasOne('App\Domaine');
    }
}
