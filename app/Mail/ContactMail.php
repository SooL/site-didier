<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\Request;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('rfejoz@gmail.com')
                    ->view('email.accuse')
                    ->with(['nom' => $this->request->nom,
                            'email' => $this->request->email,
                            'sujet' => $this->request->sujet,
                            'bodyMessage' => $this->request->message,
                          ]);
    }
}
