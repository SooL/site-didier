<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Domaine extends Model
{
    public function images()
    {
      return $this->hasMany('App\Image');
    }
}
