<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use App\Contact;

class EmailController extends Controller
{
  public function send(Request $request){
    request()->validate ([
    'nom' => 'sometimes|required|between:3,50',
    'email' => 'sometimes|required|email',
    'sujet' => 'sometimes|required|max:255',
    'message' => 'sometimes|required|max:2000',
      ]);

    $contact = new Contact;
    $contact->nom = $request->input('nom');
    $contact->email = $request->input('email');
    $contact->sujet = $request->input('sujet');
    $contact->message = $request->input('message');
    $contact->save();

    $to_email = "rfejoz@gmail.com";

    Mail::to($to_email)->send(new ContactMail($request));

    return view('accueil')->with('status', "Bien envoyé");


  }
  public function voir(){
    return view('email.send');
  }
}
