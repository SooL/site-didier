<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "DomaineController@accueil")->name('accueil');
Route::resource('domaine', 'DomaineController');
Route::resource('contact', 'ContactController');
Route::resource('image', 'ImageController');
Route::get('/voir', 'EmailController@voir')->name('voir');
Route::post('/send', 'EmailController@send')->name('send');
