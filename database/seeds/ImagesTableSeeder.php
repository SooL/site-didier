<?php

use Illuminate\Database\Seeder;
use App\Image;
class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('images')->insert([
          'url' => "https://www.quebecscience.qc.ca/wp-content/uploads/2018/10/electricien.jpg",
          'domaine_id' => '1'
          ]);
      DB::table('images')->insert([
          'url' => "https://www.objectifgard.com/wp-content/uploads/2020/03/Intervention-Electricite.jpg",
          'domaine_id' => '1'
          ]);
      DB::table('images')->insert([
          'url' => "https://www.billy-berclau.fr/images/actualites/slider/2016/07/coupure_erdf.jpg",
          'domaine_id' => '1'
          ]);

      DB::table('images')->insert([
          'url' => "https://www.belmard-batiment.fr/img/intervention-plomberie-paris-15.jpg",
          'domaine_id' => '2'
          ]);
      DB::table('images')->insert([
          'url' => "https://www.agep-plomberie.fr/userfiles/16660/PLOMBERIE-%C3%80-BASTIA12.jpg",
          'domaine_id' => '2'
          ]);
      DB::table('images')->insert([
          'url' => "https://machronique.com/wp-content/uploads/2019/10/comment-devenir-un-bon-plombier-dinstallation.jpg",
          'domaine_id' => '2'
          ]);

      DB::table('images')->insert([
          'url' => "https://cache.marieclaire.fr/data/photo/w1000_ci/5g/appliquer-peinture-au-mur.jpg",
          'domaine_id' => '3'
          ]);
      DB::table('images')->insert([
          'url' => "https://cdn-media.rtl.fr/cache/1cEwlj1JtQtK00wpIeqECA/880v587-0/online/image/2014/0820/7773799217_les-regles-d-or-pour-peindre-un-mur.jpg",
          'domaine_id' => '3'
          ]);
      DB::table('images')->insert([
          'url' => "https://lh3.googleusercontent.com/proxy/dZ4h0nlnuZXmVbYdptcc7w8lfgah8KBMZoo38V3NGvVxWgj3VPzCIgWoNn58QNd5J-NJVSVJ-V9AvzY6qPpD8lBao22hpDszjQ6VLJ1X_JZvpwotBDcGx-FriFqu1e_tysNSDqNxiJscWtaOHllaUbvbX64IJjcjA0UgX8w7",
          'domaine_id' => '3'
          ]);

      DB::table('images')->insert([
          'url' => "https://www.monequerre.fr/wp-content/uploads/2018/11/travaux-sur-les-cloisons.jpg",
          'domaine_id' => '4'
          ]);
      DB::table('images')->insert([
          'url' => "https://www.rjplatre.fr/files/site/photos/platrerie%20tradi%20article%20.jpg",
          'domaine_id' => '4'
          ]);
      DB::table('images')->insert([
          'url' => "https://lh3.googleusercontent.com/proxy/yPGfL96QjhFbohcc6HidsFM0omUfEe_C-xRrCB0GW8sdhnAAUl5rViNnRzVRMPSodR4CDk-V9X7OJ_FFj5rsXMDP2mmJ9wwVD49DMbTkZxdkFZ24c8zaEUtkxJtTXtsw9sxJuHSotCCP1wZ_Hem3c_j7mg",
          'domaine_id' => '4'
          ]);
   }

}
