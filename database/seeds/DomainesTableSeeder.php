<?php

use Illuminate\Database\Seeder;
use App\Domaine;

class DomainesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('domaines')->insert([
          'libelle' => 'Electricité',
          'url' => 'https://cdn.pixabay.com/photo/2015/12/07/10/55/electric-1080584_960_720.jpg',
      ]);
      DB::table('domaines')->insert([
          'libelle' => 'Plomberie',
          'url' => 'https://cdn.pixabay.com/photo/2015/07/11/14/53/plumbing-840835_960_720.jpg',
      ]);
      DB::table('domaines')->insert([
          'libelle' => 'Peinture',
          'url' => 'https://cdn.pixabay.com/photo/2016/10/16/11/29/painting-1744953_960_720.jpg',
      ]);
      DB::table('domaines')->insert([
          'libelle' => 'Platerie',
          'url' => 'https://live.staticflickr.com/1736/42823026912_bf8c466d83_b.jpg',
      ]);
    }
}
